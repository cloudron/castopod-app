[0.1.0]
 * Initial version for Castopod

[0.2.0]
* Enable Redis password support
* Fix email setup

[1.0.0]
* Initial stable version

[1.1.0]
* Update Castopod to 1.6.0
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.6.0)

[2.0.0]
* Update Castopod to 1.6.2
* This version brings a security patch from the upstream auth library codeigniter4/shield.
* **You will not be able to sign in to your admin, and that is normal. All of your instance's users will have to reset their password by clicking on "Use a Login Link" as if they forgot their password.**
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.6.1)
* admin: redirect root fediverse route to fediverse-blocked-actors ([ba5324e](https://code.castopod.org/adaures/castopod/commit/ba5324ea1942a3939f186e974d29fb393c54b253))
* analytics: show full referrer domain in web pages visits reports ([6be38e9](https://code.castopod.org/adaures/castopod/commit/6be38e9fda3d1436d81686e1a3a5e5b173e390a0)), closes [#367](https://code.castopod.org/adaures/castopod/issues/367)
* auth: overwrite Shield's PermissionFilter ([c6e8000](https://code.castopod.org/adaures/castopod/commit/c6e8000bab54f4a32068578f750f4cf9d91bad89))
* auth: update shield from v1.0.0-beta.3 to v1.0.0-beta.6 ([23842df](https://code.castopod.org/adaures/castopod/commit/23842df03ae28e416390e2436442b8e7c8340333))
* platforms: add missing tiktok to social platforms seed ([8dfdaf3](https://code.castopod.org/adaures/castopod/commit/8dfdaf321566050e9c53683e70864871eb55d618))
* remove fediverse prefix to prevent migration error + load routes during podcast import ([7ff1dbe](https://code.castopod.org/adaures/castopod/commit/7ff1dbe9030768074b2fe7c7f570bfb9e7336f62))
* routes: overwrite RouteCollection to include all routes + update js and php dependencies ([b4f1b91](https://code.castopod.org/adaures/castopod/commit/b4f1b916bfec53f071e8d0d900081c6d74486e53))
* update Router to include latest CI changes with alternate-content logic ([ae57601](https://code.castopod.org/adaures/castopod/commit/ae57601c838a7aa9469bae8038ac1c30d8c9a51e))
* use podcast-activity named route instead of not existing actor route ([3c35718](https://code.castopod.org/adaures/castopod/commit/3c357183ca51545787fcfc801b4a5829d9cd8ad6))
* migrations: remove if exists modifier for drop index (82013c9), closes #382

[2.0.1]
* Update Castopod to 1.6.3
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.6.3)
* fediverse: add index to post controller-method to access post's jsonld contents (35142d8)

[2.0.2]
* Update Castopod to 1.6.4
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.6.4)
* fediverse: do not cache remote action form + fix typo on post routes for passing post uuid (4ecb42f)
* fediverse: update post controller namespace in routes (3189f12)

[2.0.3]
* Update Castopod to 1.6.5
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.6.5)

[2.1.0]
* Update base image to 4.2.0

[2.2.0]
* Update Castopod to 1.7.0
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.7.0)

[2.2.1]
* Update Castopod to 1.7.1
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.7.1)
* housekeeping: add where clause to check episode_id is not null on reset comments count (119742c)

[2.2.2]
* Update Castopod to 1.7.2
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.7.2)
* render episode number optional when episode type is trailer or bonus 

[2.2.3]
* Update Castopod to 1.7.3
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.7.3)
* analytics: upgrade opawg's user-agents-php to user-agents-v2-php (8cd7886)
* platforms: add Threads and YouTube Music (9264a2d)

[2.2.4]
* Clear cache when starting up

[2.2.5]
* Update Castopod to 1.7.4
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.7.4)
* media: add missing HEAD route for static assets served with S3 (b61a32c)

[2.3.0]
* Update Castopod to 1.8.1
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.8.0)
* **admin:** add tooltip for not authorized routes ([f7f9baf](https://code.castopod.org/adaures/castopod/commit/f7f9bafc3e56621fab2569d9d76baafe0a2e940d))
* **admin:** emphasize unprivileged items in sidebar with "prohibited" icon ([0bd7dde](https://code.castopod.org/adaures/castopod/commit/0bd7ddea58adf502121b83e5c09317e20912fb4e))
* allow hiding owner's email in public RSS feed ([222e02a](https://code.castopod.org/adaures/castopod/commit/222e02a2af9ecb8b8768a63d3054f4c3ef54e991))
* **persons:** order persons by `full_name` ASC for easier list scanning ([68a599f](https://code.castopod.org/adaures/castopod/commit/68a599fee08c71763b9336e14b1c0d9e28c4449b)), closes [#418](https://code.castopod.org/adaures/castopod/issues/418)

[2.3.1]
* Update Castopod to 1.8.2
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.8.2)
* **transcript:** add condition when concatenating sub text to prevent second line duplication ([6cbfec0](https://code.castopod.org/adaures/castopod/commit/6cbfec0d7d9bf85c8014d379026648857ea13373))

[2.4.0]
* Update Castopod to 1.9.0
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.9.0)
* add actor domain to handle in follow page ([de099ac](https://code.castopod.org/adaures/castopod/commit/de099ac64300b8edb86e387fde89c0a3e9472f46))
* **admin:** add podcast's OP3 analytics dashboard link ([5f3752b](https://code.castopod.org/adaures/castopod/commit/5f3752b4430f6f2d5f9e5f6a7a003bc4d2f9d487))

[2.5.0]
* Update Castopod to 1.10.0
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.10.0)
* **op3:** move op3 prefix to enclosure url instead of audio proxy ([d580369](https://code.castopod.org/adaures/castopod/commit/d5803692357952d82d54efd8d3aa71de3a1c9571))
* **podcast-import:** rollback transaction before exception is thrown ([419bb04](https://code.castopod.org/adaures/castopod/commit/419bb04716088586b87b2c8f24a954ca8cfd6c76)), closes [#429](https://code.castopod.org/adaures/castopod/issues/429) [#319](https://code.castopod.org/adaures/castopod/issues/319) [#443](https://code.castopod.org/adaures/castopod/issues/443) [#438](https://code.castopod.org/adaures/castopod/issues/438)
* add podcast:season and podcast:episode tags to rss feed ([98c6658](https://code.castopod.org/adaures/castopod/commit/98c6658840eedd55bd6d8042f8a69c342b87cd71))
* add support for podcasting 2.0 "medium" tag with podcast, music and audiobook ([630e788](https://code.castopod.org/adaures/castopod/commit/630e788f0e1ddfe5de229bd415a8e15361efa746)), closes [#439](https://code.castopod.org/adaures/castopod/issues/439)
* display chapters in episode's public page ([87cc437](https://code.castopod.org/adaures/castopod/commit/87cc437e1ead5486ed46ca37e2055aaf5c9445c1)), closes [#423](https://code.castopod.org/adaures/castopod/issues/423)
* support VTT transcript file format in addition to SRT ([7071b4b](https://code.castopod.org/adaures/castopod/commit/7071b4b6f48cb9a2f766064f3a5c23f92b293718)), closes [#433](https://code.castopod.org/adaures/castopod/issues/433)

[2.5.1]
* Update Castopod to 1.10.1
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.10.1)
* fediverse: use config name to get Fediverse config properties instead of hardcoded class string (5fd0980)

[2.5.2]
* Update Castopod to 1.10.2
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.10.2)
* podcast-import: move closing parenthasis when checking for owner name and email existence (cec7815)

[2.5.3]
* Update Castopod to 1.10.3
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.10.3)
* **chapters:** use episode cover when chapter img is an empty string ([a343de4](https://code.castopod.org/adaures/castopod/commit/a343de4cf6ba38561b8fe675fa9c38d9f0ecfec7)), closes [#444](https://code.castopod.org/adaures/castopod/issues/444)
* **import:** set episodes as premium if podcast is set as premium by default ([dfd66be](https://code.castopod.org/adaures/castopod/commit/dfd66beebfcca1670b0a9d389e8e3f8d2d08d2f2))

[2.5.4]
* Update Castopod to 1.10.4
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.10.4)
* display chapters in episode preview page (797516a), closes #445

[2.5.5]
* Update Castopod to 1.10.5
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.10.5)
* file-uploads: validate chapters json content + remove permit_empty rule to uploaded files (6289c42), closes #445

[2.6.0]
* Update Castopod to 1.11.0
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.11.0)
* set itunes:block on premium feeds to prevent indexing ([88851b0](https://code.castopod.org/adaures/castopod/commit/88851b022663d575a816f0e2f33f0353767dd52d))
* generate podcast guid if empty ([a5aef2a](https://code.castopod.org/adaures/castopod/commit/a5aef2a63e464632f3941649d455672835989e6c)), closes [#450](https://code.castopod.org/adaures/castopod/issues/450)
* add trailer tags to rss if trailer episodes are present ([80fdd9c](https://code.castopod.org/adaures/castopod/commit/80fdd9cfb4a95feac6ed0000435a013fc83e6892))
* add transcript display to episode page ([4d141fc](https://code.castopod.org/adaures/castopod/commit/4d141fceae56fa9e666b42c32a830ff9c68989db)), closes [#411](https://code.castopod.org/adaures/castopod/issues/411)
* add telegram to socials ([004f804](https://code.castopod.org/adaures/castopod/commit/004f804045cd8e884361bb4318109fbdd7afc9a8))
* add truefans.fm and episodes.fm ([d046ecc](https://code.castopod.org/adaures/castopod/commit/d046ecc52f6ccd41d09f6de48e00d2c61d25d7f0)), closes [#458](https://code.castopod.org/adaures/castopod/issues/458) [#459](https://code.castopod.org/adaures/castopod/issues/459)

[2.6.1]
* Add email display name support

[2.7.0]
* Update Castopod to 1.12.0
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.0)

[2.7.1]
* Update Castopod to 1.12.1
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.1)

[2.7.2]
* Update Castopod to 1.12.2
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.2)

[2.7.3]
* Update Castopod to 1.12.3
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.3)

[2.7.4]
* Update Castopod to 1.12.4
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.4)
* icons: set correct names for lock and lock-unlock icons in premium banner (94deaab)
* premium-podcasts: update query to validate subscription (0e6d294)

[2.7.5]
* Update Castopod to 1.12.5
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.5)
* rss: add subscription id to cache name to prevent premium feeds from overlapping (5310d86)

[2.7.6]
* Update Castopod to 1.12.6
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.6)

[2.7.7]
* Update Castopod to 1.12.7
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.7)

[2.7.8]
* Update Castopod to 1.12.8
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.8)
* podcast-model: always query podcast from database when clearing cache (995ca5b)

[2.7.9]
* Update Castopod to 1.12.9
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.9)
* fediverse: add "processing" and "failed" statuses to better manage broadcast load (cf9e072), closes #511

[2.7.10]
* Update Castopod to 1.12.10
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.10)

[2.7.11]
* Update Castopod to 1.12.11
* [Full changelog](https://github.com/ad-aures/castopod/releases/tag/v1.12.11)

[2.8.0]
* Update castopod to 1.13.0
* [Full Changelog](https://github.com/ad-aures/castopod/releases/tag/v1.13.0)

[2.8.1]
* Update castopod to 1.13.1
* [Full Changelog](https://github.com/ad-aures/castopod/releases/tag/v1.13.1)

[2.8.2]
* Update castopod to 1.13.2
* [Full Changelog](https://github.com/ad-aures/castopod/releases/tag/v1.13.2)

[2.8.3]
* Update castopod to 1.13.3
* [Full Changelog](https://github.com/ad-aures/castopod/releases/tag/v1.13.3)

[2.8.4]
* Use CLOUDRON_PROXY_IP

[2.9.0]
* Add checklist

[2.9.1]
* Update castopod to 1.13.4
* [Full Changelog](https://github.com/ad-aures/castopod/releases/tag/v1.13.4)

