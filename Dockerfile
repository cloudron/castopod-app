FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get remove -y php-* php8.1-* libapache2-mod-php8.1 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt-get update && \
    apt install -y php8.3 php8.3-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.3 && \
    apt install -y php-{date,pear,twig,validate} && \
    apt-get install -y --no-install-recommends gettext-base libjpeg62-dev libwebp-dev libxpm-dev libpcre2-8-0 && \
    rm -rf /var/lib/apt/lists/*

# this binaries are not updated with PHP_VERSION since it's a lot of work
RUN update-alternatives --set php /usr/bin/php8.3 && \
    update-alternatives --set phar /usr/bin/phar8.3 && \
    update-alternatives --set phar.phar /usr/bin/phar.phar8.3 && \
    update-alternatives --set phpize /usr/bin/phpize8.3 && \
    update-alternatives --set php-config /usr/bin/php-config8.3

ARG COMPOSER_VERSION=2.8.5
RUN curl --fail -sS https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar -o /usr/local/bin/composer && chmod +x /usr/local/bin/composer

# renovate: datasource=gitlab-releases depName=adaures/castopod versioning=semver extractVersion=^v(?<version>.+)$ registryUrl=https://code.castopod.org
ARG CASTOPOD_VERSION=1.13.4

RUN curl -L --fail https://code.castopod.org/adaures/castopod/-/archive/v${CASTOPOD_VERSION}/castopod-v${CASTOPOD_VERSION}.tar.gz | tar -xz --strip-components=1 -C /app/code

# update seed file with admin user details (email: admin@cloudron.local / password: changeme)
# php -r "echo password_hash('changeme', PASSWORD_DEFAULT, ['cost' => 10]) . \"\n\";"
# https://code.castopod.org/adaures/castopod/-/blob/develop/app/Database/Seeds/DevSuperadminSeeder.php?ref_type=heads#L35
RUN sed -e "s,admin@castopod.local,admin@cloudron.local,g" \
        -e "s,castopod,changeme,g" \
        -i /app/code/app/Database/Seeds/DevSuperadminSeeder.php

RUN composer update && \
    npm install && \
    npm install --global pnpm

ENV NODE_ENV production
RUN npm run build && \
    npm run build:static

# we can also use media.root and media.storage to move the public/media directory elsewhere
RUN mv /app/code/writable /app/code/writable.orig && ln -sf /app/data/writable /app/code/writable && \
    mv /app/code/public/media /app/code/public/media.orig && ln -sf /app/data/media /app/code/public/media && \
    ln -sf /run/castopod/env /app/code/.env

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log && a2enmod rewrite env && a2dismod perl
COPY apache/castopod.conf /etc/apache2/sites-enabled/castopod.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_size 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP max_execution_time 900 && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP file_uploads On && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP max_input_time 900

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
