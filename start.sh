#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /run/castopod

[[ ! -d /app/data/writable ]] && cp -r /app/code/writable.orig /app/data/writable
[[ ! -d /app/data/media ]] && cp -r /app/code/public/media.orig /app/data/media

if [[ ! -f /app/data/env ]]; then
    export ANALYTICS_SALT=$(openssl rand -base64 32)
    echo -e "# https://code.castopod.org/adaures/castopod/-/blob/develop/env \nanalytics.salt="${ANALYTICS_SALT}"\nlogger.threshold = 4" > /app/data/env
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Update configuration"
# first user configs then extend with cloudron one
cat /app/data/env > /run/castopod/env
# initial newlines are important in case /app/data/env does not have a newline in the end!
cat >> /run/castopod/env <<EOF


app.baseURL="${CLOUDRON_APP_ORIGIN}"
app.proxyIPs.0="${CLOUDRON_PROXY_IP}"
media.baseURL="${CLOUDRON_APP_ORIGIN}"
admin.gateway="cp-admin"
auth.gateway="cp-auth"

database.default.hostname="${CLOUDRON_MYSQL_HOST}"
database.default.port="${CLOUDRON_MYSQL_PORT}"
database.default.database="${CLOUDRON_MYSQL_DATABASE}"
database.default.username="${CLOUDRON_MYSQL_USERNAME}"
database.default.password="${CLOUDRON_MYSQL_PASSWORD}"
database.default.DBPrefix="cp_"

cache.handler="redis"
cache.redis.host="${CLOUDRON_REDIS_HOST}"
cache.redis.password="${CLOUDRON_REDIS_PASSWORD}"
cache.redis.port=${CLOUDRON_REDIS_PORT}
cache.redis.database=0
email.protocol="smtp"
email.SMTPHost="${CLOUDRON_MAIL_SMTP_SERVER}"
email.SMTPUser="${CLOUDRON_MAIL_SMTP_USERNAME}"
email.SMTPPass="${CLOUDRON_MAIL_SMTP_PASSWORD}"
email.fromEmail="${CLOUDRON_MAIL_FROM}"
email.fromName="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Castopod}"
email.SMTPPort=${CLOUDRON_MAIL_SMTP_PORT}
email.SMTPCrypto=""
EOF

readonly table_count=$(mysql -NB -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" "${CLOUDRON_MYSQL_DATABASE}" 2>/dev/null)

if [[ "${table_count}" == "0" ]]; then
    echo "==> initialize DB on first run"

    # https://github.com/ad-aures/castopod/blob/main/docs/src/contributing/setup-development.md
    /usr/bin/php /app/code/spark migrate -all
    /usr/bin/php /app/code/spark db:seed AppSeeder

    echo "==> Setting admin user up (email: admin@cloudron.local / password: changeme)"
    /usr/bin/php /app/code/spark db:seed DevSuperadminSeeder
else
    echo "==> DB migration"
    /usr/bin/php /app/code/spark migrate -all
fi

echo "==> Clearing cache"
/usr/bin/php /app/code/spark cache:clear

echo "==> Changing ownership"
chown -R www-data:www-data /app/data /run/castopod

echo "==> Starting Castopod"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
