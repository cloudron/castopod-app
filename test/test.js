#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const toCamelCase = (str) => str.toLowerCase().replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) => idx === 0 ? ltr.toLowerCase() : ltr.toUpperCase()).replace(/\s+/g, '');

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_EMAIL='admin@cloudron.local';
    const ADMIN_PASSWORD='changeme';
    const LOCATION = process.env.LOCATION || 'test';
    const PODCAST_NAME = 'cloudron podcast ' + Math.floor((Math.random() * 100) + 1);
    const PODCAST_HANDLE = toCamelCase(PODCAST_NAME);
    const PODCAST_OWNER = 'tester';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(email, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/cp-auth/login');
        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();
        await browser.sleep(3000);

        await waitForElement(By.xpath('//h1[contains(text(), "Admin dashboard")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/cp-auth/logout');
        await browser.sleep(3000);
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//h1[contains(text(), "Login")]'));
    }

    async function podcastExists() {
        //await browser.get('https://' + app.fqdn + '/cp-admin/podcasts');
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);

        if (await browser.findElements(By.xpath(`//h3[contains(., "${PODCAST_NAME}")]`)).then(found => !!found.length)) {
            await browser.findElement(By.xpath(`//h3[contains(., "${PODCAST_NAME}")]/ancestor::a`)).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath(`//h1[contains(., "${PODCAST_NAME}")]`));

        // click Play button inside shadow content
        if (await browser.findElements(By.xpath('//play-episode-button')).then(found => !!found.length)) {
            var playbutton = await browser.findElement(By.xpath('//play-episode-button'));
            var shadow_root = await playbutton.getShadowRoot();
            if (await shadow_root.findElements(By.css('button')).then(found => !!found.length)) {
                var button = await shadow_root.findElement(By.css('button'));
                // move mouse to click
                await browser.actions({async: true}).move({origin: button}).click().perform();
                await browser.sleep(15000);
            }
        }
    }

    async function createPodcast() {
        await browser.get(`https://${app.fqdn}/cp-admin/podcasts/new`);

        await waitForElement(By.xpath('//input[@name="cover"]'));
        await browser.findElement(By.xpath('//input[@name="cover"]')).sendKeys(path.resolve('./podcast_cover.jpg'));
        await browser.findElement(By.xpath('//input[@name="title"]')).sendKeys(`${PODCAST_NAME}`);
        await browser.findElement(By.xpath('//input[@name="handle"]')).sendKeys(`${PODCAST_HANDLE}`);
        await browser.findElement(By.xpath('//textarea[@name="description"]')).sendKeys(`${PODCAST_NAME}`);
        await browser.findElement(By.xpath('//input[@name="owner_name"]')).sendKeys(`${PODCAST_OWNER}`);
        await browser.findElement(By.xpath('//input[@name="owner_email"]')).sendKeys(`${ADMIN_EMAIL}`);
        await browser.findElement(By.xpath('//button[contains(., "Create podcast")]')).click();

        await waitForElement(By.xpath('//a[contains(., "Add an episode")]'));
        await browser.findElement(By.xpath('//a[contains(., "Add an episode")]')).click();
        await browser.findElement(By.xpath('//input[@name="audio_file"]')).sendKeys(path.resolve('./podcast_episode.mp3'));
        await browser.findElement(By.xpath('//input[@name="title"]')).sendKeys(`${PODCAST_NAME} episode 1`);
        await browser.findElement(By.xpath('//textarea[@name="description"]')).sendKeys(`${PODCAST_NAME} episode 1`);
        await browser.findElement(By.xpath('//button[contains(., "Create episode")]')).click();

        await waitForElement(By.xpath('//a[contains(., "Publish")]'));
        await browser.findElement(By.xpath('//a[contains(., "Publish")]')).click();

        await waitForElement(By.xpath('//textarea[@name="message"]'));
        await browser.findElement(By.xpath('//textarea[@name="message"]')).sendKeys(`${PODCAST_NAME} episode 1 announcement`);
        await browser.findElement(By.xpath('//button[contains(., "Publish")]')).click();
        await browser.sleep(5000);
    }

    async function publishPodcast() {
        await browser.get(`https://${app.fqdn}/cp-admin/podcasts/1/publish`);

        await waitForElement(By.xpath('//textarea[@name="message"]'));
        await browser.findElement(By.xpath('//textarea[@name="message"]')).sendKeys(`${PODCAST_NAME} announcement`);
        await browser.findElement(By.xpath('//input[@id="now"]')).click();
        await browser.findElement(By.xpath('//button[contains(., "Publish")]')).click();
        await browser.sleep(5000);
    }

    async function checkPodcastPublished() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath(`//h3[contains(., "${PODCAST_NAME}")] | //h1[contains(., "${PODCAST_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('can create podcast', createPodcast);
    it('can publish podcast', publishPodcast);
    it('did publish podcast', checkPodcastPublished);
    it('check podcast exist', podcastExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check podcast exist', podcastExists);
    it('did publish podcast', checkPodcastPublished);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check podcast exist', podcastExists);
    it('did publish podcast', checkPodcastPublished);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check podcast exist', podcastExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id org.castopod.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can create podcast', createPodcast);
    it('publish podcast', publishPodcast);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check podcast exist', podcastExists);
    it('did publish podcast', checkPodcastPublished);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
