## About

Castopod is a free and open-source podcast hosting solution made for podcasters who want engage and interact with their audience.

## Features

* Talk to your audience, directly
* Promote your podcast
* Your podcast, everywhere
* Built-in analytics 

